﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMotor : MonoBehaviour {

	[SerializeField]
	private Camera cam;

	private Vector3 velocity = Vector3.zero;
	private Vector3 rotation = Vector3.zero;
	private Vector3 camrotation = Vector3.zero;

	private Rigidbody rb;

	void Start () {
		rb = GetComponent<Rigidbody>();	
	}
	
	public void Move(Vector3 _velocity) {
		velocity = _velocity;
	}

	public void Rotate(Vector3 _rotation) {
		rotation = _rotation;
	}

	public void RotateCamera(Vector3 _camrotation) {
		camrotation = _camrotation;
	}

	void FixedUpdate () {
		rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
		rb.MoveRotation(rb.rotation * Quaternion.Euler(rotation));
		cam.transform.Rotate(-camrotation);
	}

}
